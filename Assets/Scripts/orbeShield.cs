﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbeShield : MonoBehaviour
{
    [SerializeField]
    private Player m_player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            m_player.m_shield += 10;
            Destroy(gameObject);
        }
    }
}
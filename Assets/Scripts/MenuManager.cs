﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
	[SerializeField] private Button m_btnPlay;
	[SerializeField] private Button m_btnOpt;
	[SerializeField] private Button m_btnQuit;

	[SerializeField] private Text m_txtRes;
	[SerializeField] private Dropdown m_dropRes;
	[SerializeField] private Text m_txtVol;
	[SerializeField] private Slider m_slidVol;
	[SerializeField] private Button m_btnBck;

    // Start is called before the first frame update
    void Start()
    {
		m_txtRes.gameObject.SetActive(false);
		m_dropRes.gameObject.SetActive(false);
		m_txtVol.gameObject.SetActive(false);
		m_slidVol.gameObject.SetActive(false);
		m_btnBck.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void SetRes()
	{
		if(m_dropRes.value == 0)
		{
			Screen.SetResolution(1920, 1080, true);
		}else{
			if(m_dropRes.value == 1)
				Screen.SetResolution(1440, 900, true);
			else
				Screen.SetResolution(1024, 768, true);
		}
	}

	public void LaunchPreGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

    public void LaunchGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
	
	public void GameOption()
	{
		m_txtRes.gameObject.SetActive(true);
		m_dropRes.gameObject.SetActive(true);
		m_txtVol.gameObject.SetActive(true);
		m_slidVol.gameObject.SetActive(true);
		m_btnBck.gameObject.SetActive(true);

		m_btnPlay.gameObject.SetActive(false);
		m_btnOpt.gameObject.SetActive(false);
		m_btnQuit.gameObject.SetActive(false);
	}

	public void BackGameOption()
	{
		m_txtRes.gameObject.SetActive(false);
		m_dropRes.gameObject.SetActive(false);
		m_txtVol.gameObject.SetActive(false);
		m_slidVol.gameObject.SetActive(false);
		m_btnBck.gameObject.SetActive(false);

		m_btnPlay.gameObject.SetActive(true);
		m_btnOpt.gameObject.SetActive(true);
		m_btnQuit.gameObject.SetActive(true);
	}

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LaunchMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }
}

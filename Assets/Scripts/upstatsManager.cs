﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class upstatsManager : MonoBehaviour
{
	[SerializeField] private Player player;

	[SerializeField] private GameObject m_upStatsPanel;
	[SerializeField] private Text m_txtHealth;
	[SerializeField] private Text m_txtShield;
	[SerializeField] private Text m_txtKills;
	[SerializeField] private Text m_txtLevel;
	[SerializeField] private Text m_txtCoins;
	[SerializeField] private Text m_txtXP;

	private bool m_activUpStatsPanel = false;

    // Start is called before the first frame update
    void Start()
    {
		m_upStatsPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			if(m_activUpStatsPanel == false)
			{
				m_activUpStatsPanel = true;
				m_upStatsPanel.SetActive(true);
                Time.timeScale = 0;
			}
			else
			{
				m_activUpStatsPanel = false;
				m_upStatsPanel.SetActive(false);
                Time.timeScale = 1;
            }
				
		}

		m_txtHealth.text = "Max Health : " + player.m_maxHealth.ToString();
		m_txtShield.text = "Max Shield : " + player.m_maxShield.ToString();
		m_txtKills.text = "Kills Counter : " + player.m_kills.ToString();
		m_txtLevel.text = "Current Level : " + player.m_level.ToString();
		m_txtCoins.text = "Clérencoins : " + player.m_coins.ToString();
		m_txtXP.text = "XP : " + player.m_xp.ToString() + "/100";
    }

	public void UpHealth()
	{
		if(player.m_coins >= 1)
		{
			player.m_coins--;
			player.m_maxHealth += 25;
            player.m_health = player.m_maxHealth;
		}
	}

	public void UpShield()
	{
		if (player.m_coins >= 1)
		{
			player.m_coins--;
			player.m_maxShield += 25;
		}
	}
}

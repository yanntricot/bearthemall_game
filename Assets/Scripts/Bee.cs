﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : MonoBehaviour
{
	public short m_health;
	[SerializeField]
	protected float m_speed;
	public float m_stoppingDistance;
	public float m_retreatDistance;
	protected bool m_nrv = false; // = true si c'est la reine en phase 2

	public float m_timeBtwshots;
	protected float m_startTimeBtwShots;
    private float m_accuracy;
	private float m_bulletForce;

	public GameObject projectile;
	public Transform m_firePoint;
	protected Transform player;

    protected GameObject m_playerPref;
	protected Player m_playerCode;

	// Start is called before the first frame update
	virtual protected void Start()
    {
        m_accuracy = Random.Range(0f, 0.3f);
        m_startTimeBtwShots = Random.Range(2f, 3f);
        m_bulletForce = Random.Range(11f, 17f);
        m_speed = Random.Range(5f, 8f);

        player = GameObject.FindGameObjectWithTag("Player").transform;
		m_playerPref = GameObject.FindGameObjectWithTag("Player");
		m_playerCode = m_playerPref.GetComponent("Player") as Player;

		m_timeBtwshots = m_startTimeBtwShots;
	}

    // Update is called once per frame
    virtual protected void Update()
    {
		transform.eulerAngles = new Vector3(0, 0, -transform.eulerAngles.z);
		transform.LookAt(player.position, transform.up);
		transform.Rotate(new Vector3(0, -90, 0), Space.Self); //correcting the original rotation 

		if (Vector2.Distance(transform.position, player.position) >= m_stoppingDistance)
		{
			transform.position = Vector2.MoveTowards(transform.position, player.position, m_speed * Time.deltaTime);
		}
		else if(Vector2.Distance(transform.position, player.position) <= m_stoppingDistance && Vector2.Distance(transform.position, player.position) >= m_retreatDistance)
		{
			transform.position = this.transform.position;
		}
		else if (Vector2.Distance(transform.position, player.position) <= m_retreatDistance)
		{
			transform.position = Vector2.MoveTowards(transform.position, player.position, -m_speed * Time.deltaTime);
		}

		if(m_timeBtwshots <= 0) // gestion du tir
		{
			GameObject bullet1 = Instantiate(projectile, m_firePoint.position + (m_firePoint.up / 4), m_firePoint.rotation * Quaternion.Euler(0f, 0f, (float)Random.Range(-20 * (1 - m_accuracy), 20 * (1 - m_accuracy)))); // balle centrale
			Rigidbody2D rb1 = bullet1.GetComponent<Rigidbody2D>();
			rb1.AddForce(bullet1.transform.right * m_bulletForce, ForceMode2D.Impulse);
			if (m_nrv == true)
			{
				GameObject bullet2;
				Rigidbody2D rb2;
				for (int i = 0; i < 2; i++)
				{
					bullet2 = Instantiate(projectile, m_firePoint.position + (m_firePoint.up / 4), m_firePoint.rotation * Quaternion.Euler(0f, 0f, (float)Random.Range(-20 * (1 - m_accuracy), 20 * (1 - m_accuracy))));
					rb2 = bullet2.GetComponent<Rigidbody2D>();
					rb2.AddForce(bullet2.transform.right * m_bulletForce, ForceMode2D.Impulse);
				}
			}
			m_timeBtwshots = m_startTimeBtwShots;
		}
		else
		{
			m_timeBtwshots -= Time.deltaTime;
		}

        if (m_health <= 0) { Destroy(gameObject); m_playerCode.m_kills++; m_playerCode.m_xp += 25; }
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		//Debug.Log("Collision avec " + col.gameObject.tag);
		if (col.gameObject.tag == "bullet")
		{
			Destroy(col.gameObject);
			if (m_health - 12 > 0) m_health -= 12; else m_health = 0;
			//Debug.Log("Enemy got hit - life left > " + m_health);
		}
		else
		{
			//Debug.Log("1er else");
			if (col.gameObject.tag == "pellet")
			{
				Destroy(col.gameObject);
				if (m_health - 5 > 0) m_health -= 5; else m_health = 0;
				//Debug.Log("Enemy got hit - life left > " + m_health);
			}
			else
			{
				//Debug.Log("2nd else");
				if (col.gameObject.tag == "explosion")
				{
					if (m_health - 80 > 0) m_health -= 80; else m_health = 0;
					//Debug.Log("Enemy got hit - life left > " + m_health);
				}
			}
		}		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Character
{
    [SerializeField]
    private Transform player;
	[SerializeField]
	private Player bear;

	public float m_stoppingDistance;
	public float m_retreatDistance;

	private void followPlayer()
    {
		/*if(Vector2.Distance(transform.position, player.position) >= m_stoppingDistance)
		{
			transform.position = Vector2.MoveTowards(transform.position, player.position, m_speed * Time.deltaTime);

		}else */if(Vector2.Distance(transform.position, player.position) <= m_stoppingDistance && Vector2.Distance(transform.position, player.position) >= m_retreatDistance){

			transform.position = this.transform.position;

		}else if(Vector2.Distance(transform.position, player.position) <= m_retreatDistance)
		{
			transform.position = Vector2.MoveTowards(transform.position, player.position, -m_speed * Time.deltaTime);
		}
		else { transform.position = Vector2.MoveTowards(transform.position, player.position, m_speed * Time.deltaTime); }
	}

	public void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
    protected override void Update()
    {
        followPlayer();
		if (m_health <= 0) { Destroy(gameObject); bear.m_kills++; bear.m_xp += 25; };
        base.Update();
    }
}
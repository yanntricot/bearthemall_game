﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
	[SerializeField] private GameObject m_spawners;
	private List<GameObject> m_listSpawners = new List<GameObject>();

	[SerializeField] public GameObject m_bee;
	private byte m_nbBee;
	private byte m_lvlWave;
	private float m_fLvlWave;
	private float m_minBee;

	[SerializeField] private Text m_txtTimeUntilWave;
	[SerializeField] private Text m_txtRemainingWave;
	private float m_timeUntilWave;
	private bool m_waveLaunch;
	private int m_succedWave;

	// Start is called before the first frame update
	void Start()
	{
		m_listSpawners.AddRange(GameObject.FindGameObjectsWithTag("spawn"));
		m_lvlWave = 1;
		m_waveLaunch = true;
		m_timeUntilWave = 30;
		m_succedWave = 0;
	}

	// Update is called once per frame
	void Update()
	{
		if(m_succedWave == 6)
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

		if(m_waveLaunch == true)
		{
			foreach (GameObject spawn in m_listSpawners)
			{
				if (m_lvlWave < 10) { m_fLvlWave = (float)m_lvlWave; } else { m_fLvlWave = 10.0f; }

				m_nbBee = (byte)Random.Range(m_fLvlWave*2, 11.5f*2);

				for (int index = 0; index < m_nbBee*2; index++)
					Instantiate(m_bee, spawn.transform);
			}m_waveLaunch = false;
		}
		else
		{
			m_timeUntilWave += Time.deltaTime;

			if(m_timeUntilWave >= 30)
			{
				m_waveLaunch = true;
				m_succedWave ++;
				m_timeUntilWave = 0;
			}
		}
		string timer = ((int)30 - m_timeUntilWave).ToString("f2");
		m_txtTimeUntilWave.text =  timer + "s";
		m_txtRemainingWave.text = "Wave "+ m_succedWave +"/5";
	}
}

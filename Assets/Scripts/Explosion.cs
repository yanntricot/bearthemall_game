﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public byte damage = 100;
    public float size = 1;
    private float timer = 0;

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Explosion sur " + col.gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (size < 5)
        {
            timer += Time.deltaTime;
            size = 12.5f * timer + 1;
            damage = (byte)(-200 * timer + 100);
            gameObject.transform.localScale = new Vector3(size, size, 1);
        }
        else Destroy(gameObject);
    }
}

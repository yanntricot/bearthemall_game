﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
	public Transform m_firePoint;
	public GameObject m_bulletPrefab;
	public GameObject m_pelletPrefab;
	public GameObject m_grenadePrefab;
	public GameObject m_rocketPrefab;
	private GameObject m_whatToShoot;
	private float m_bulletForce;

	[SerializeField]
	private Image m_reloadImg;
	private float m_timeUnitilLaunchG;
	[SerializeField]
	private float m_timeUnitilLaunchR;
	private byte m_grenadeLaunch = 0;
	[SerializeField]
	private byte m_rocketlaunch = 0;
	private bool m_canGrenade = true;
	[SerializeField]
	private bool m_canRocket = true;

	[SerializeField] private Text m_txtArmeSelec;
	public byte m_weapon = 1; // 1=pistol, 2=shotgun, 3=assault_rifle, 4=grenade_launcher, 5=rocket_launcher

	IEnumerator Mitraillette()
	{
		while (Input.GetButton("Fire1"))
		{
			yield return new WaitForSeconds(0.08f);
			Shoot();
		}
	}

	private void Shoot()
	{
		byte m_nbBullets;
		float m_accuracy; // 0 = nul, 1 = parfait

		switch (m_weapon)
		{
			case 1: //pistol
				m_nbBullets = 1;
				m_whatToShoot = m_bulletPrefab;
				m_accuracy = 0.9f;
				m_bulletForce = 20f;
				break;

			case 2: //shotgun
				m_nbBullets = 7;
				m_whatToShoot = m_pelletPrefab;
				m_accuracy = 0.1f;
				m_bulletForce = 30f;
				break;

			case 3: //assault rifle
				m_nbBullets = 1;
				m_whatToShoot = m_bulletPrefab;
				m_accuracy = 0.8f;
				m_bulletForce = 20f;
				break;

			case 4: //grenade launcher
				m_nbBullets = 1;
				m_whatToShoot = m_grenadePrefab;
				m_accuracy = 0.7f;
				m_bulletForce = 10f;
				break;

			case 5: //rocket launcher
				m_nbBullets = 1;
				m_whatToShoot = m_rocketPrefab;
				m_accuracy = 0.95f;
				m_bulletForce = 10f;
				break;

			default:
				m_nbBullets = 0;
				m_whatToShoot = m_rocketPrefab;
				m_accuracy = 0;
				m_bulletForce = 100f;
				break;
		}
		GameObject bullet;
		Rigidbody2D rb;
		if((m_weapon == 4 && m_canGrenade == true) || (m_weapon == 5 && m_canRocket == true) || m_weapon == 1 || m_weapon == 2 || m_weapon == 3)
		{
			for (byte i = 0; i < m_nbBullets; i++)
			{
				bullet = Instantiate(m_whatToShoot, m_firePoint.position + (m_firePoint.up / 4), m_firePoint.rotation * Quaternion.Euler(0f, 0f, (float)Random.Range(-20 * (1 - m_accuracy), 20 * (1 - m_accuracy))));
				rb = bullet.GetComponent<Rigidbody2D>();
				rb.AddForce(bullet.transform.up * m_bulletForce, ForceMode2D.Impulse);
			}
			if(m_weapon == 4 && m_rocketlaunch >= 5) m_canGrenade = false;
			if(m_weapon == 5 && m_rocketlaunch >= 1) m_canRocket = false;
		}
	}

	// Start is called before the first frame update
	private void Start()
    {
		m_txtArmeSelec.text = "HandGun";
		m_reloadImg.gameObject.SetActive(false);
	}

    // Update is called once per frame
    private void Update()
    {
		/*MouseScroll && TextManagement*/
		#region
		if (Input.mouseScrollDelta.y > 0)
		{
			if(m_weapon <= 0)
			{
				m_weapon = 5;
			}
			else
			{
				if(m_weapon >= 5)
				{
					m_weapon = 1;
				}
				else { m_weapon++; }
			}
		}
		else
		{
			if(Input.mouseScrollDelta.y < 0)
			{
				m_weapon--;
				if (m_weapon <= 0)
				{
					m_weapon = 5;
				}
				else
				{
					if (m_weapon >= 5)
					{
						m_weapon = 1;
					}
				}
			}
		}

		if (m_weapon == 1)
		{
			m_txtArmeSelec.text = "HandGun";
		}
		else
		{
			if (m_weapon == 2)
			{
				m_txtArmeSelec.text = "Shotgun";
			}
			else
			{
				if (m_weapon == 3)
				{
					m_txtArmeSelec.text = "Assault Rifle";
				}
				else
				{
					if (m_weapon == 4)
					{
						m_txtArmeSelec.text = "Grenade Launcher";
						m_reloadImg.gameObject.SetActive(true);
						m_reloadImg.fillAmount = m_timeUnitilLaunchG;
					}
					else
					{
						m_txtArmeSelec.text = "Rocket Launcher";
						m_reloadImg.gameObject.SetActive(true);
						m_reloadImg.fillAmount = m_timeUnitilLaunchR;
					}
				}
			}
		}
		#endregion  
		/*=============================*/

		if (Input.GetButtonDown("Fire1"))
		{
			if (m_weapon == 4)
			{
				m_grenadeLaunch++;
			}
			else
			{
				if (m_weapon == 5)
				{
					m_rocketlaunch++;
				}
			}

			if (m_weapon == 3)
			{
				StartCoroutine(Mitraillette());
			}
			else Shoot();
		}

		if (m_grenadeLaunch >= 5)
		{
			m_canGrenade = false;
			m_timeUnitilLaunchG += Time.deltaTime;
		}

		if (m_rocketlaunch >= 1)
		{
			m_canRocket = false;
			m_timeUnitilLaunchR += Time.deltaTime;
		}

		if (m_timeUnitilLaunchG >= 1)
		{
			m_canGrenade = true;
			m_grenadeLaunch = 0;
			m_timeUnitilLaunchG = 0;
		}

		if (m_timeUnitilLaunchR >= 1)
		{
			m_canRocket = true;
			m_rocketlaunch = 0;
			m_timeUnitilLaunchR = 0;
		}
	}
}

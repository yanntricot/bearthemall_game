﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    public Transform player;
    private Vector2 direction;
    public float speed = 0;
    private Player joueur;
    //private Vector2 mousePos;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //mousePos = joueur.mousePos;
        //direction = ((Vector2)player.position + mousePos)/ 2 - mousePos;
        direction = (player.position - transform.position) / (1/speed);
        Debug.DrawLine(transform.position, player.position, Color.gray);

        transform.Translate(direction);
        //transform.Translate(direction * speed * Time.deltaTime);
    }
}

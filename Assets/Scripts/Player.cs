﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : Character
{
	/// <summary>
	/// Procedure which detects user directional inputs via keyboard keys or a joystick,
	/// and changes the direction of the player following the latest inputs.
	/// </summary>
	/// 
	public float m_moveSpeed = 5f;
	public Rigidbody2D rb;
	public Camera cam;
	Vector2 movement;
	public Vector2 mousePos;

	// Stats Player
	public byte m_level = 0;
	public float m_shield = 5;
	public float m_maxShield = 25;
	public int m_kills = 0;
	public byte m_xp = 0;
	public byte m_coins = 0;

    [SerializeField]
    protected Slider m_healthBar;
	[SerializeField]
    protected Slider m_shieldBar;

	public void MinHealth(byte p_damage)
	{
		if (m_health <= p_damage)
		{
			m_health = 0;
			// TODO appel die();
		}
		else { m_health -= p_damage; }
		m_healthBar.value = m_health;
	}

	public void PlusHealth(byte p_heal)
	{
		if (m_health + p_heal > 100)
		{
			m_health = 100;
			// TODO add buff, extra shield (possibility)
		}
		else { m_health += p_heal; }
		m_healthBar.value = m_health;
	}

	private void Start()
	{
		m_healthBar.value = m_health;
		m_shieldBar.value = m_shield;
	}

	protected override void Update()
    {
		movement.x = Input.GetAxisRaw("Horizontal");
		movement.y = Input.GetAxisRaw("Vertical");

		mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

		if(m_xp >= 100)
		{
			m_level++;
			m_coins++;
			m_xp = 0;
		}

		m_healthBar.maxValue = m_maxHealth;
        m_healthBar.value = m_health;
		m_shieldBar.maxValue = m_maxShield;
        m_shieldBar.value = m_shield;

		if (m_shield < m_maxShield) m_shield += Time.deltaTime;
        base.Update();
    }

	protected void FixedUpdate()
	{
		rb.MovePosition(rb.position + movement * m_moveSpeed * Time.fixedDeltaTime);
        Vector2 lookDir = mousePos - rb.position;
		float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
		rb.rotation = angle;
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "enemyBullet")
        {
            Destroy(col.gameObject);
            if (m_shield - 3 > 0)
            {
                m_shield -= 3; 
            }
            else if (m_health - 2 > 0)
            {
                m_shield = 0;
                m_health -= 2; Debug.Log("Player get hit - life left > " + m_health);
            }
            else { 
				m_health = 0; 
				SceneManager.LoadScene("MainMenu");
				Destroy(gameObject); 
			}
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	private float m_autoDestruct;
	public Rigidbody2D rb;
	[SerializeField]
	public byte damage = 15;
	public GameObject m_explosionPrefab;

	// Start is called before the first frame update
	void Start()
    {
		//rb.velocity = transform.right * speed * Time.deltaTime;
		if (gameObject.tag == "grenade")
			StartCoroutine("Explode");
	}

    // Update is called once per frame
    void Update()
    {
		m_autoDestruct += Time.deltaTime;
		if (m_autoDestruct >= 3) Destroy(gameObject);
    }

	void OnTriggerEnter2D(Collider2D col)
	{
		//Debug.Log("Hit");
		if (col.gameObject.tag == "destroyBullet" || col.gameObject.tag == "Player") Destroy(gameObject);
	}

	IEnumerator Explode()
	{
		yield return new WaitForSeconds(2);
		Instantiate(m_explosionPrefab, transform.position, transform.rotation);
		Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (gameObject.tag != "grenade")

		{
			if (gameObject.tag == "rocket")
				Instantiate(m_explosionPrefab, transform.position, transform.rotation);
			Destroy(gameObject);
		}
	}
}

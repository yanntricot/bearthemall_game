﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BossBee : Bee
{
    [SerializeField]
    private Transform m_beeRotatePoint;
    [SerializeField]
    private byte m_state = 0; // = 1 for second phase
    [SerializeField]
    protected Slider m_healthBar;
    [SerializeField]
    protected Text m_txtSuccess;

    // Start is called before the first frame update
    override protected void Start()
    {
        // boosts its turret bees' health
        m_beeRotatePoint.GetChild(0).GetComponent<Bee>().m_health *= 5;
        m_beeRotatePoint.GetChild(1).GetComponent<Bee>().m_health *= 5;
        // adjusts is moving speed
        base.Start();
        m_speed = 3f;
        // adjusts its shooting speed
        m_startTimeBtwShots = Random.Range(0.3f, 0.9f);
        m_timeBtwshots = m_startTimeBtwShots;
    }

    ~BossBee()
    {
        Destroy(m_healthBar.gameObject);
        m_txtSuccess.enabled = true;
    }

    private void badTime()
    {
        Debug.Log("It's bad time, time.");
        m_state = 1;
        m_nrv = true;
        m_startTimeBtwShots = Random.Range(0.1f, 0.5f);
        m_speed = 4f;
        m_stoppingDistance = 5;
        m_retreatDistance = 4;
    }

    // Update is called once per frame
    override protected void Update()
    {
        // make the bees turn around the queen bee
        m_beeRotatePoint.RotateAround(m_beeRotatePoint.position, new Vector3(0, 0, 1), 60 * Time.deltaTime);

        // if there isn't a turret bee left
        if (m_beeRotatePoint.childCount < 1 && m_state == 0)
        {
            badTime();
        }

        m_healthBar.value = m_health;

        if(m_health == 0){
            Destroy(m_healthBar.gameObject);
            m_txtSuccess.enabled = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 3);
        }
        base.Update();
    }
}
